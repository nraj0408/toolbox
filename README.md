## Contributor Success toolbox scripts

## Setup

```shell
› bundle install
```

### A note about tokens

Certain scripts may require a `read_api` access a token. This should be stored according to [the Token Management Standard](https://about.gitlab.com/handbook/security/token-management-standard.html#token-storage), ideally in [1Password](https://about.gitlab.com/handbook/security/password-guidelines.html#1password).

To access 1Password via CLI look here: https://developer.1password.com/docs/cli/get-started#install

You could dynamically pull this out of your 1Password vault with a command like the following. (This assumes a credential called "my gitlab pat" in the "Private" vault, with a "credential" value)

```shell
bundle exec bin/community_mr_participants.rb -s 2023-07-03 -e 2023-07-05 -w -t $(op read "op://Private/my gitlab pat/credential")
```

## History

This was forked from https://gitlab.com/gitlab-org/quality/toolbox on 2023-03-31 using:

```shell
git clone git@gitlab.com:gitlab-org/quality/toolbox.git
cd toolbox
git filter-repo --path .gitlab/ --path .gitignore --path .gitlab-ci.yml --path .rubocop.yml --path .rubocop_todo.yml --path CONTRIBUTING.md --path Gemfile --path Gemfile.lock --path LICENSE --path README.md --path bin/community_mr_participants.rb
git branch -m master main
```

As a result of using [`git filter-repo`](https://github.com/newren/git-filter-repo) we retained all change history on the files we kept, while removing all history of the other files from the original repo. 

## Scripts

### Community MR Participants

This script analyses all merged `Community contribution` MRs between the dates and
outputs a slack message recognising team members that have interacted with the wider
community. The slack message can be pasted into `#thanks` in slack.

This script is used as a part of the [Contributor Success Thanks message](https://www.gitlab.com/handbook/marketing/community-relations/contributor-success/community-contributors-workflows.html#contributor-thanks-messages) workflow.

```shell
> bundle exec bin/community_mr_participants.rb --help
Usage: bin/community_mr_participants.rb [options]

    -s, --startdate START_DATE       The start point for your search. yyyy-mm-dd (e.g. 2022-12-05)
    -e, --enddate END_DATE           The end point for your search. Only merge requests merged BEFORE this date will be included. Default: startdate + 7 days
    -w, --wider                      Run report for wider community
    -t, --token access_token         A valid access token (necessary when using -w)
    -p, --disable-paging             Disable paging - will only execute a single query. Default: false
    -o, --sheet-output               Enable sheet (CSV) output. Default: false
    -c, --page-size count            Number of items to fetch per query. Default: 25
    -d, --debug                      Print debugging information
    -h, --help                       Print help message

Example usage:
- Weekly team member report (run on Monday, replacing yyyy-mm-dd with last Monday's date):
    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd
- Monthly wider community report (run on the 22nd, replacing yyyy-mm-dd with the 22nd of last month, and YYYY-MM-DD with the 22nd of this month):
    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd -e YYYY-MM-DD -w -t $GITLAB_API_READ_TOKEN
```

