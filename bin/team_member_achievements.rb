#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'yaml'
require 'json'
require 'pry'

require_relative '../lib/team_yml_helper'

# Grab all team members
# Grab all user_achievements for the team member achievement

# Award the achievement to anyone who has not got it but is a team member
# Revoke the achievement from anyone who has it but is not a team mbmer
class TeamMemberAchievements
  include TeamYml

  GRAPHQL_API_URL = ENV.fetch('CI_API_GRAPHQL_URL')
  NAMESPACE_FULL_PATH = 'gitlab-org/achievements'
  TEAM_ACHIEVEMENT_ID = 58
  GITLAB_API_TOKEN = ENV.fetch('GITLAB_API_TOKEN')
  DRY_RUN = ENV.fetch('DRY_RUN', '1')

  def exclude_member?(item)
    return true if item['role'] == 'Core Team member'

    super
  end

  def execute_graphql(query)
    cookie_hash = HTTParty::CookieHash.new
    cookie_hash.add_cookies('gitlab_canary=true')
    response = HTTParty.post(
      GRAPHQL_API_URL,
      body: { query: query }.to_json,
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{GITLAB_API_TOKEN}", 'Cookie' => cookie_hash.to_cookie_string }
    )
    JSON.parse(response.body)
  end

  def current_achievement_users
    users = []
    after = nil
    loop do
      puts 'grabbing a page of existing user achievements...'
      result = get_achievement_users_page(after)
      after = result[:end_cursor]
      users.concat(result[:users])

      break unless result[:has_next_page]
    end
    users
  end

  def get_achievement_users_page(after)
    query = <<~EOGQL
      query getAchievements {
        namespace(fullPath: "#{NAMESPACE_FULL_PATH}") {
          achievements(ids: "gid://gitlab/Achievements::Achievement/#{TEAM_ACHIEVEMENT_ID}") {
            nodes {
              userAchievements(
                after: "#{after}"
                ) {
                nodes {
                  id
                  user {
                    username
                  }
                }
                pageInfo {
                  hasNextPage
                  endCursor
                }
              }
            }
          }
        }
      }
    EOGQL

    response = execute_graphql(query)
    user_achievements = response.dig('data', 'namespace', 'achievements', 'nodes')[0]['userAchievements']
    users = user_achievements['nodes'].map { |ua| { 'id' => ua['id'], 'username' => ua.dig('user', 'username').downcase } }
    puts "Got #{users.count} from #{users.first['username']} to #{users.last['username']}"
    has_next_page = user_achievements.dig('pageInfo', 'hasNextPage')
    end_cursor = user_achievements.dig('pageInfo', 'endCursor')

    { users: users, has_next_page: has_next_page, end_cursor: end_cursor }
  end

  def revoke_achievements(user_achievement_gids)
    user_achievement_gids.each do |gid|
      query = <<~EOGQL
        mutation revokeAchievement {
          achievementsRevoke(input: {
            userAchievementId: "#{gid}" }) {
              userAchievement {
                user {
                  username
                }
              }
            errors
          }
        }
      EOGQL

      if DRY_RUN == '0'
        puts execute_graphql(query)
      else
        puts "Revoking #{gid}"
      end
    end
  end

  def get_user_id(username)
    query = <<~EOGQL
      query getUser {
        user(username: "#{username}") {
          id
        }
      }
    EOGQL
    execute_graphql(query).dig('data', 'user', 'id')
  end

  def award_achievements(usernames)
    usernames.each do |username|
      user_gid = get_user_id(username)
      query = <<~EOGQL
        mutation awardAchievement {
          achievementsAward(input: {
            achievementId: "gid://gitlab/Achievements::Achievement/#{TEAM_ACHIEVEMENT_ID}",
            userId: "#{user_gid}" }) {
              userAchievement {
                user {
                  username
                }
              }
              errors
          }
        }
      EOGQL

      if DRY_RUN == '0'
        puts execute_graphql(query)
      else
        puts "Award #{username}"
      end
    end
  end

  def execute
    should_have_achievement = team_from_www.keys
    have_achievement = current_achievement_users

    to_award_usernames = should_have_achievement - have_achievement.map { |u| u['username'] }
    puts 'to_award_usernames:'
    puts to_award_usernames
    award_achievements(to_award_usernames)

    to_revoke = have_achievement.map { |u| u['username'] } - should_have_achievement
    puts 'to_revoke:'
    puts to_revoke

    to_revoke_gids = to_revoke.map { |t| have_achievement.find { |h| h['username'] == t }['id'] }
    puts 'to_revoke_gids:'
    puts to_revoke_gids
    revoke_achievements(to_revoke_gids)
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  TeamMemberAchievements.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
