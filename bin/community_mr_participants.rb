#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'csv'
require 'optparse'
require 'optparse/time'
require 'graphql/client'
require 'graphql/client/http'
require 'httparty'
require 'pp'
require 'yaml'
require 'erb'
require 'pry'
require 'gitlab'

require_relative '../lib/team_yml_helper'

Options = Struct.new(
  :token,
  :start_date,
  :end_date,
  :disable_paging,
  :page_size,
  :sheet_output,
  :debug,
  :wider_community,
  :scheduled
)

def parse_args(argv) # rubocop:disable Metrics/MethodLength
  args = Options.new

  OptionParser.new do |opts| # rubocop:disable Metrics/BlockLength
    opts.banner = "Usage: #{__FILE__} [options]\n\n"

    opts.on('-s', '--startdate START_DATE', Time, 'The start point for your search. yyyy-mm-dd (e.g. 2022-12-05)') do |value|
      args.start_date = value
    end

    opts.on('-e', '--enddate END_DATE', Time, 'The end point for your search. Only merge requests merged BEFORE this date will be included. Default: startdate + 7 days') do |value|
      args.end_date = value
    end

    opts.on('-w', '--wider', 'Run report for wider community') do |value|
      args.wider_community = value
    end

    opts.on('-t', '--token access_token', String, 'A valid access token (necessary when using -w)') do |value|
      args.token = value
    end

    opts.on('-p', '--disable-paging', 'Disable paging - will only execute a single query. Default: false') do |value|
      args.disable_paging = value
    end

    opts.on('-o', '--sheet-output', 'Enable sheet (CSV) output. Default: false') do |value|
      args.sheet_output = value
    end

    opts.on('-c', '--page-size count', Integer, 'Number of items to fetch per query. Default: 25') do |value|
      args.page_size = value
    end

    opts.on('-d', '--debug', 'Print debugging information') do |value|
      args.debug = value
    end

    opts.on('-h', '--help', 'Print help message') do
      $stdout.puts opts
      $stdout.puts "\nExample usage:"
      $stdout.puts "- Weeky team member report (run on Monday, replacing yyyy-mm-dd with last Monday's date):"
      $stdout.puts '    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd'
      $stdout.puts '- Monthly wider community report (run on the 22nd, replacing yyyy-mm-dd with the 22nd of last month, and YYYY-MM-DD with the 22nd of this month):'
      $stdout.puts '    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd -e YYYY-MM-DD -w -t $GITLAB_API_READ_TOKEN'
      exit
    end
  end.parse!(argv)

  if ENV.fetch('COMMUNITY_MR_PARTICIPANTS', '0') == '1' || ENV.fetch('WIDER_MR_PARTICIPANTS', '0') == '1'
    args.scheduled = true
    args.token = ENV.fetch('COMMUNITY_MR_BOT_TOKEN')
    args.start_date = Time.parse(Date.today.to_s) - 7 * 86_400
    args.wider_community = true if ENV.fetch('WIDER_MR_PARTICIPANTS', '0') == '1'
  end

  parse_args(%w[-h]) if args.start_date.nil?
  parse_args(%w[-h]) if args.wider_community && !args.token

  args.end_date = args.start_date + 7 * 86_400 if args.end_date.nil?
  args.page_size = 25 if args.page_size.nil?

  args
end

class CommunityMrParticipants # rubocop:disable Metrics/ClassLength
  include TeamYml

  Client = GraphQL::Client

  GITLAB_API = 'https://gitlab.com/api/graphql'
  GITLAB_REST_API = 'https://gitlab.com/api/v4'
  GITLAB_COM_GROUP_ID = 6543
  LABEL_COMMUNITY = 'Community contribution'
  USER_AGENT = "GitLab Quality Toolbox #{`git show --pretty=%H -q`.chomp}"
  CONTRIBUTOR_SUCCESS_PROJECT_ID = 39_971_471
  GITLAB_CONTRACTOR_GROUP_ID = 62_281_442
  TEAM_MEMBER_ISSUE_ID = 85
  WIDER_ISSUE_ID = 186

  # Known users that we want to disregard (automation and otherwise)
  IGNORE_ACCOUNTS = %w[
    employment-bot
    ghost1
    gitlab-bot
    gitlab-dependency-bot
    gitlab-dependency-update-bot
    gitlab-infra-mgmt-bot
    gitlab-jh-bot
    gitlab-qa
    gitlab-release-tools-bot
    kubitus-bot
    GitLabReviewerRecommenderBot
    Taucher2003-Bot
    mr-bot
    ops-gitlab-net
    GitLab-Llm-Bot
  ].freeze
  GITLAB_SERVICE_ACCOUNT_REGEX = /^((project|group)_\d+_bot(\w+)?|gl-service-([-\w])+)$/.freeze

  SLACK_MESSAGE = "
    ** Paste this message into slack using Cmd-v, then apply formatting using Cmd-Shift-F **

    :heart: :tada: Weekly Wider Contribution Appreciation :tada: :heart:
    We are striving toward 1000 unique monthly wider community contributors and appreciate every effort supporting our wider community.
    If you have any ideas/feedback/concerns please head over to #contributor-success.

    Thank you to all <%= leaders.length() %> GitLab team members who helped get one of the <%= mrs.length() %> MRs merged from <%= from_date %> to <%= to_date %>.

    Top performers (3+ interactions! We're also mentioning your managers to increase awareness of these awesome results :rocket:)
    <%= top_performers.sort.join(', ') %>


    High performers (2 interactions!)
    <%= high_performers.sort.join(', ') %>


    Thank you to these wonderful additional team members for helping our community members in the past week (1 interaction)

    <%= additional_mentions.sort.join(', ') %>


    cc @Sid, @Ashley, @esalvador, @John Coghlan, @Nick Veenhof
    cc (Managers of Top Performers :rocket:) <%= manager_mentions.sort.join(', ') %>
  ".gsub(/^    /, '')

  DISCORD_MESSAGE = "
    ** Paste this message into discord using Cmd-v/Ctrl-V **
    :heart: :tada: Community Contribution Appreciation :tada: :heart:
    We are striving toward 1000 unique monthly wider community contributors and appreciate every effort from the community towards this goal.
    If you have any ideas/feedback/concerns please feel free to discuss them here!

    Thank you to all <%= author_leaders.length() %> wider community members who **AUTHORED** merge requests that were merged from <%= from_date %> to <%= to_date %>.
    There were a total of <%= authored_mrs.length() %> community contributions!

    Top authors (3+ merge requests :rocket:)
    <%= author_performers[:top_performers].sort.join(', ') %>


    Regular authors (2 merge requests!)
    <%= author_performers[:high_performers].sort.join(', ') %>


    Additional authors (1 merge request)
    <%= author_performers[:additional_mentions].sort.join(', ') %>


    ---
    Additionally, thank you to all <%= participant_leaders.length() %> wider community members who participated/reviewed _other_ merge requests, merged from <%= from_date %> to <%= to_date %>.

    Top performers (3+ interactions :rocket:)
    <%= participant_performers[:top_performers].sort.join(', ') %>


    High performers (2 interactions!)
    <%= participant_performers[:high_performers].sort.join(', ') %>


    Additional contributors (1 interaction)
    <%= participant_performers[:additional_mentions].sort.join(', ') %>
  ".gsub(/^    /, '')

  def initialize(options)
    @token = options.token
    @start_date = options.start_date
    @end_date = options.end_date
    @debug_status = options.debug
    @disable_paging = options.disable_paging
    @page_size = options.page_size
    @sheet_output = options.sheet_output
    @wider_community = options.wider_community
    @author_count = 0
    @mr_count = 0
    @reviewer_count = 0
    @reviewer_mr_count = 0
  end

  def execute
    merge_requests = []
    count = 0
    # TODO: Consider adding `gitlab-community` at a later date (we don't currently use the `Community contribution` label, so it won't work at the moment)
    %w[gitlab-com gitlab-org].each do |group_path|
      query_vars = {
        from_date: @start_date,
        to_date: @end_date,
        end_cursor: '',
        page_size: @page_size,
        labels: @wider_community ? nil : [LABEL_COMMUNITY],
        group_path: group_path
      }
      loop do
        print '.'
        query_result = query(query_to_execute, resource_path: [], variables: query_vars)

        # is there a cleaner way?
        merge_requests += query_result.dig(:results, 'group', 'mergeRequests', 'edges')
        query_vars[:end_cursor] = query_result[:end_cursor] if query_result[:more_pages]
        count += 1

        break unless query_result[:more_pages]
        break if @disable_paging
      end
    end

    print_query(count, merge_requests)
  end

  def print_query(count, merge_requests)
    print "\n"
    puts "[DEBUG] Query executed #{count} times total" if @debug_status
    puts "[DEBUG] #{merge_requests.length} results total" if @debug_status

    processed_mrs = process(merge_requests)

    sheet_display(processed_mrs) if @sheet_output

    if @wider_community
      build_wider_report(processed_mrs)
    else
      build_team_report(processed_mrs)
    end
  end

  def query_to_execute
    <<~GRAPHQL
      query($from_date: Time, $to_date: Time, $end_cursor: String, $page_size: Int, $labels: [String!], $group_path: ID!) {
        group(fullPath: $group_path) {
          mergeRequests(
            state: merged,
            mergedAfter: $from_date,
            mergedBefore: $to_date,
            first: $page_size,
            after: $end_cursor,
            labels: $labels
          ) {
            edges {
              node {
                iid
                title
                mergedAt
                author {
                  name
                  username
                }
                webUrl
                labels {
                  nodes {
                    title
                  }
                }
                reviewers {
                  nodes {
                    name
                    username
                  }
                }
                approvedBy {
                  nodes {
                    name
                    username
                  }
                }
                commenters {
                  nodes {
                    name
                    username
                  }
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    GRAPHQL
  end

  def update_issue(message, issue)
    message = "```\n#{message}\n```"
    if @wider_community
      additional_description = "\n| #{@end_date.strftime('%Y-%m-%d')} | #{@mr_count} | #{@author_count} | #{@reviewer_mr_count} | #{@reviewer_count} |"
      issue_id = WIDER_ISSUE_ID
    else
      additional_description = "\n- [ ] #{@end_date.strftime('%Y-%m-%d')} - (#{@reviewer_count} GitLab team members helped #{@mr_count} MRs)"
      issue_id = TEAM_MEMBER_ISSUE_ID
    end
    gitlab_client.create_issue_note(CONTRIBUTOR_SUCCESS_PROJECT_ID, issue_id, message)
    update_issue_description(issue, additional_description)
  end

  def update_issue_description(issue, additional_description)
    current_description = issue.description
    new_description = current_description + additional_description
    gitlab_client.edit_issue(issue.project_id, issue.iid, { description: new_description })
  end

  def get_issue(project_id, issue_id)
    gitlab_client.issue(project_id, issue_id)
  end

  def post_to_slack(message, issue)
    message_body <<~MESSAGE
      #{issue.web_url}
      ```
      #{message}
      ```
    MESSAGE
    HTTParty.post(
      ENV.fetch('SLACK_CONTRIBUTOR_SUCCESS_WEBHOOK_URL'),
      body: { text: message_body }.to_json
    )
  end

  private

  def process(merge_requests)
    processed_mrs = []
    merge_requests.each do |mr_node|
      mr_details = mr_node['node']

      mr_result = {
        iid: mr_details['iid'],
        url: mr_details['webUrl'],
        user_name: mr_details['author']['username'],
        merge_request_title: mr_details['title'],
        merged_at: mr_details['mergedAt'],
        reviewers: get_unique_participants(mr_details['reviewers']),
        approvers: get_unique_participants(mr_details['approvedBy']),
        comment_authors: get_unique_participants(mr_details['commenters'])
      }
      mr_result[:author] = { mr_details['author']['username'] => mr_details['author']['name'] }
      mr_result[:all_participants] = mr_result[:reviewers].merge(mr_result[:approvers]).merge(mr_result[:comment_authors]).merge(mr_result[:author])
      mr_result[:team_participants] = extract_team_participants(mr_result) unless @wider_community
      mr_result[:wider_community_participants] = extract_wider_community(mr_result) if @wider_community
      processed_mrs.push(mr_result)
    end
    processed_mrs
  end

  def get_unique_participants(participants)
    unique_participants = {}
    participants['nodes'].each do |node|
      next if node.nil?

      if node.key?('name')
        unique_participants[node['username']] = node['name']
      elsif node.key?('author')
        unique_participants[node['author']['username']] = node['author']['name']
      else
        puts "[DEBUG] #{pp participants}" if @debug_status
        raise 'Unknown participants hash!'
      end
    end
    unique_participants
  end

  def extract_team_participants(merge_request)
    team_participants = {}
    merge_request[:all_participants].each do |k, v|
      team_participants[k] = v if team_members.key?(k)
    end
    team_participants
  end

  def extract_wider_community(merge_request)
    wider_community = {}
    merge_request[:all_participants].each do |k, v|
      next if not_wider_community?(k)

      wider_community[k] = v
      add_wider_community_members(k, v)
    end
    wider_community
  end

  def not_wider_community?(user)
    team_members.key?(user.downcase) || ignorable_account?(user)
  end

  def ignorable_account?(username)
    IGNORE_ACCOUNTS.include?(username) ||
      username.match?(GITLAB_SERVICE_ACCOUNT_REGEX) ||
      dynamic_ignore_list?(username)
  end

  def dynamic_ignore_list?(username)
    return @dynamic_ignore[username] if @dynamic_ignore&.key?(username)

    @dynamic_ignore ||= {}

    @dynamic_ignore[username] = ex_team_member?(username) || gitlab_contractor?(username)
  end

  def ex_team_member?(username)
    # can we find the username? if not -> blocked
    user = gitlab_client.user_search(nil, username: username)

    user.empty?
  end

  def gitlab_contractor?(username)
    gitlab_contractor_members.include?(username)
  end

  def gitlab_contractor_members
    @gitlab_contractor_members ||= populate_contractor_members
  end

  def populate_contractor_members
    members = gitlab_client.group_members(GITLAB_CONTRACTOR_GROUP_ID, { per_page: 100 })
    members.map(&:username)
  end

  def gitlab_client
    @gitlab_client ||= Gitlab.client(endpoint: GITLAB_REST_API, private_token: @token)
  end

  def sheet_display(mrs)
    puts('MERGED_AT,URL,USER_NAME,MERGE_REQUEST_TITLE,GitLab Team members')

    mrs.each do |mr|
      print("#{mr[:merged_at]},")
      print("#{mr[:url]},")
      print("#{mr[:user_name]},")
      print("#{mr[:merge_request_title]},")
      mr[:team_participants].each do |k, _v|
        print("#{mr[:team_participants][k]},")
      end
      print("\n")
    end
  end

  def build_team_report(mrs)
    leaders = count_participants(mrs, :team_participants)

    from_date = @start_date.strftime('%Y-%m-%d')
    to_date = @end_date.strftime('%Y-%m-%d')
    top_performers = []
    high_performers = []
    additional_mentions = []
    manager_mentions = []

    leaders.each do |k, v|
      leader_item = team_members[k]
      leader_handle = "@#{leader_item[:name]}"
      if v >= 3
        leader_manager = get_name_from_slug(leader_item)
        top_performers.push(leader_handle)
        manager_mentions.push("@#{leader_manager}") unless leader_manager.nil?
      elsif v == 2
        high_performers.push(leader_handle)
      else
        additional_mentions.push(leader_item[:name])
      end
    end
    manager_mentions = manager_mentions.uniq

    @mr_count = mrs.length
    @reviewer_count = leaders.length
    message = ERB.new(SLACK_MESSAGE, trim_mode: '>')
    message.result(binding)
  end

  def build_wider_report(mrs)
    puts "Received #{mrs.length} merge requests"
    participated_mrs = mrs.select { |mr| mr[:wider_community_participants].any? }
    authored_mrs = mrs.reject { |mr| not_wider_community?(mr[:user_name]) }
    puts "#{participated_mrs.length} of which contain wider community participants"
    puts "and #{authored_mrs.length} were authored by wider community members"
    participant_leaders = count_participants(participated_mrs, :wider_community_participants)
    author_leaders = count_participants(authored_mrs, :author)

    from_date = @start_date.strftime('%Y-%m-%d')
    to_date = @end_date.strftime('%Y-%m-%d')
    top_performers = []
    high_performers = []
    additional_mentions = []

    participant_performers = count_leaders(participant_leaders)
    author_performers = count_leaders(author_leaders)
    @author_count = author_leaders.length
    @mr_count = authored_mrs.length
    @reviewer_count = participant_leaders.length
    @reviewer_mr_count = participated_mrs.length
    message = ERB.new(DISCORD_MESSAGE, trim_mode: '>')
    message.result(binding)
  end

  def count_leaders(leaders)
    results = {
      top_performers: [],
      high_performers: [],
      additional_mentions: []
    }
    leaders.each do |k, v|
      leader_ident = "#{wider_community_members[k]} (`@#{k}`)"
      if v >= 3
        results[:top_performers].push(leader_ident)
      elsif v == 2
        results[:high_performers].push(leader_ident)
      else
        results[:additional_mentions].push(leader_ident)
      end
    end
    results
  end

  def get_name_from_slug(user)
    manager_name = user[:reports_to]

    return if manager_name.nil?

    managers = team_members.select { |_username, userdata| userdata[:slug] == manager_name }
    begin
      managers.each_value.next[:name]
    rescue StopIteration => _e
      puts "Cannot find manager (#{manager_name}) for user '#{user[:username]}'"
    end
  end

  def count_participants(mrs, sym)
    leaders = {}
    mrs.each do |mr|
      mr[sym].each do |k, _v|
        if leaders.key?(k)
          leaders[k] += 1
        else
          leaders[k] = 1
        end
      end
    end
    leaders
  end

  def query(base_query, resource_path: [], variables: {})
    graphql_query = client.parse(base_query)
    response = client.query(graphql_query, variables: variables, context: { token: @token })

    raise_on_error!(response, graphql_query, variables)

    parsed_response = parse_response(response, resource_path)
    headers = response.extensions.fetch('headers', {})

    if parsed_response.to_h.dig('group', 'mergeRequests', 'pageInfo').nil?
      pp response.to_h if @debug_status
      pp variables if @debug_status
      raise 'One of the queries timed out. Please try again with a smaller page size.'
    else
      pagination = parsed_response.to_h.dig('group', 'mergeRequests', 'pageInfo')
    end

    graphql_response = {
      ratelimit_remaining: headers['ratelimit-remaining'].to_i,
      ratelimit_reset_at: Time.at(headers['ratelimit-reset'].to_i),
      more_pages: pagination['hasNextPage'],
      end_cursor: pagination['endCursor']
    }

    return graphql_response.merge(results: {}) if parsed_response.nil?

    graphql_response.merge(results: parsed_response.to_h)
  end

  def parse_response(response, resource_path)
    resource_path.reduce(response.data) { |data, resource| data&.send(resource) }
  end

  def raise_on_error!(response, graphql_query, variables)
    return if response.errors.blank?

    puts graphql_query
    puts variables
    puts "[DEBUG] #{response.inspect}" if @debug_status

    raise "There was an error: #{response.errors.messages.to_json}"
  end

  def http_client
    Client::HTTP.new(GITLAB_API) do
      def execute(document:, operation_name: nil, variables: {}, context: {}) # rubocop:disable Lint/NestedMethodDefinition
        body = {}
        body['query'] = document.to_query_string
        body['variables'] = variables if variables.any?
        body['operationName'] = operation_name if operation_name

        response = HTTParty.post(
          uri,
          body: body.to_json,
          headers: {
            'User-Agent' => USER_AGENT,
            'Content-type' => 'application/json',
            'PRIVATE-TOKEN' => context[:token]
          }
        )

        case response.code
        when 200, 400
          JSON.parse(response.body).merge('extensions' => { 'headers' => response.headers })
        else
          { 'errors' => [{ 'message' => "#{response.code} #{response.message}" }] }
        end
      end
    end
  end

  def schema
    @schema ||= Client.load_schema(http_client)
  end

  def client
    @client ||= Client.new(schema: schema, execute: http_client).tap { |client| client.allow_dynamic_queries = true }
  end

  def add_wider_community_members(username, name)
    @wider_community_members ||= {}
    @wider_community_members[username] = name
  end

  def wider_community_members
    @wider_community_members ||= {}
  end
end

if $PROGRAM_NAME == __FILE__
  options = parse_args(ARGV)
  start = Time.now
  engine = CommunityMrParticipants.new(options)
  message = <<~EOMESSAGE
    Community MR Participants
    From:       #{options.start_date}
    To:         #{options.end_date}
    CSV output: #{options.sheet_output ? 'true' : 'false'}
    Page Size:  #{options.page_size}
    Paging:     #{!options.disable_paging}
    Debug mode: #{options.debug ? 'true' : 'false'}
    Scope:      #{options.wider_community ? 'Wider Community mode' : 'GitLab Team mode'}
  EOMESSAGE

  message += engine.execute
  message += "\nDone in #{Time.now - start} seconds."

  puts message

  return unless options.scheduled

  issue_id = options.wider_community ? WIDER_ISSUE_ID : TEAM_MEMBER_ISSUE_ID
  issue = engine.get_issue(CONTRIBUTOR_SUCCESS_PROJECT_ID, issue_id)

  engine.update_issue(message, issue)
  engine.post_to_slack(message, issue) if ENV.fetch('SLACK_CONTRIBUTOR_SUCCESS_WEBHOOK_URL', '') != ''
end
