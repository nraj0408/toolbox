#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'pry-byebug'

# Grab all recently merged MRs and their authors along with the total number of MRs their author
# has merged (in each group)

# Award the correct achievements to anyone who has not already got them
# Revoke old achievemnts from anyone who has "leveled up"
class ContributorAchievements
  GRAPHQL_API_URL = ENV.fetch('CI_API_GRAPHQL_URL')
  GITLAB_API_TOKEN = ENV.fetch('GITLAB_API_TOKEN')
  DRY_RUN = ENV.fetch('DRY_RUN', '1')
  FROM_DATE = ENV.fetch('FROM_DATE', (DateTime.now - 2).strftime[..9])
  CONTRIBUTOR_ACHIEVEMENTS = %w[
    gid://gitlab/Achievements::Achievement/59
    gid://gitlab/Achievements::Achievement/60
    gid://gitlab/Achievements::Achievement/61
    gid://gitlab/Achievements::Achievement/62
  ].freeze
  GROUPS = %w[gitlab-org gitlab-com gitlab-community].freeze

  def execute_graphql(query)
    cookie_hash = HTTParty::CookieHash.new
    cookie_hash.add_cookies('gitlab_canary=true')
    response = HTTParty.post(
      GRAPHQL_API_URL,
      body: { query: query }.to_json,
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{GITLAB_API_TOKEN}", 'Cookie' => cookie_hash.to_cookie_string }
    )
    JSON.parse(response.body)
  end

  def recent_merged_merge_requests
    users = {}
    GROUPS.each do |group|
      puts "Grabbing data from #{group}"
      after = nil
      loop do
        results = merged_merge_requests_page(group, after)
        merge_requests = results.dig('data', 'group', 'mergeRequests', 'nodes')
        merge_requests.map { |node| node['author'] }.each do |user|
          user_id = user['id']
          authored_merge_requests = user.dig('org', 'count').to_i +
                                    user.dig('com', 'count').to_i +
                                    user.dig('community', 'count').to_i
          users[user_id] = {
            id: user_id,
            authored_merge_requests: authored_merge_requests,
            achievements: user.dig('userAchievements', 'nodes')
          }
        end

        page_info = results.dig('data', 'group', 'mergeRequests', 'pageInfo')
        break unless page_info['hasNextPage']

        after = page_info['endCursor']
      end
      print "\n"
    end
    users
  end

  def merged_merge_requests_page(group, after, try = 1) # rubocop:disable Metrics/MethodLength
    print '.'
    query = <<~EOGQL
      query {
        group(fullPath: "#{group}") {
          mergeRequests(
            labels: ["Community contribution"]
            mergedAfter: "#{FROM_DATE}"
            after: "#{after}"
            first: 10
            ) {
            nodes {
              author {
                id
                username
                org: authoredMergeRequests(
                  state: merged
                  groupId: "gid://gitlab/Group/9970") {
                  count
                }
                com: authoredMergeRequests(
                  state: merged
                  groupId: "gid://gitlab/Group/6543") {
                  count
                }
                community: authoredMergeRequests(
                  state: merged
                  groupId: "gid://gitlab/Group/60717473") {
                  count
                }
                userAchievements {
                  nodes {
                    id
                    achievement {
                      id
                      name
                    }
                  }
                }
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    EOGQL

    response = execute_graphql(query)
    if try < 5 && response['errors']
      puts response
      merged_merge_requests_page(group, after, try + 1)
    else
      response
    end
  end

  def award_achievement(user_gid, achievement_id)
    query = <<~EOGQL
      mutation awardAchievement {
        achievementsAward(input: {
          achievementId: "#{achievement_id}",
          userId: "#{user_gid}" }) {
            errors
        }
      }
    EOGQL

    if DRY_RUN == '0'
      puts execute_graphql(query)
    else
      puts "Award #{achievement_id}"
    end
  end

  def revoke_achievement(user_achievement_id)
    query = <<~EOGQL
      mutation revokeAchievement {
        achievementsRevoke(input: { userAchievementId: "#{user_achievement_id}" }) {
          errors
        }
      }
    EOGQL

    if DRY_RUN == '0'
      puts execute_graphql(query)
    else
      puts "Revoke #{user_achievement_id}"
    end
  end

  def current_achievement_id(user)
    contributor_achievements = user[:achievements].select { |node| CONTRIBUTOR_ACHIEVEMENTS.include?(node.dig('achievement', 'id')) }

    { id: contributor_achievements[0]['id'], achievement_id: contributor_achievements[0]['achievement']['id'] } if contributor_achievements.any?
  end

  def determine_correct_achievement(authored_merge_requests)
    return 'gid://gitlab/Achievements::Achievement/62' if authored_merge_requests > 75 # Level 4
    return 'gid://gitlab/Achievements::Achievement/61' if authored_merge_requests > 25 # Level 3
    return 'gid://gitlab/Achievements::Achievement/60' if authored_merge_requests > 3 # Level 2

    'gid://gitlab/Achievements::Achievement/59' # Level 1
  end

  def execute
    users = recent_merged_merge_requests
    users.to_a.each do |user_gid, user|
      correct_achievement_id = determine_correct_achievement(user[:authored_merge_requests])
      existing_achievement = current_achievement_id(user)

      puts "User: #{user_gid}, should have: #{correct_achievement_id}, currently has: #{existing_achievement&.[](:achievement_id)}"

      next if correct_achievement_id == existing_achievement&.[](:achievement_id)

      award_achievement(user_gid, correct_achievement_id)
      revoke_achievement(existing_achievement[:id]) if existing_achievement
    end
    puts users.count
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  ContributorAchievements.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
