#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'

# Visits all community merge requests updated recently and adds the ~linked-issue label where appropriate.
# It would be nice if we could make this a reactive processor in triage-ops but we currently need to
# make an "expensive" API call.
# If the logic to extract issue references from the description was simpler, or the field(s) was present
# in the hook payload, we'd be onto a winner!
class MrLinkedIssueLabeller
  attr_accessor :group_full_path, :updated_after, :error

  def initialize(group_full_path, updated_after)
    @group_full_path = group_full_path
    @updated_after = updated_after
    @error = false
  end

  GITLAB_API_TOKEN = ENV.fetch('GITLAB_API_TOKEN', '')
  PRODUCTION = ENV.fetch('PRODUCTION', '') == 'true'
  REST_API_URL = ENV.fetch('CI_API_V4_URL')
  GRAPHQL_API_URL = ENV.fetch('CI_API_GRAPHQL_URL')
  # We use a generic label to identify merge requests which are linked to issues
  LINKED_ISSUE_LABEL = 'linked-issue'

  def graphql_query
    <<~EOGQL
      query {
        group(fullPath: "#{group_full_path}") {
          mergeRequests(
            # We only evaluate merge requests with the ~"Community contribution" label which have recently been updated
            labels: ["Community contribution"]
            updatedAfter: "#{updated_after.iso8601}"
            ) {
            count
            nodes {
              projectId
              iid
              webUrl
              labels {
                nodes {
                  title
                }
              }
            }
          }
        }
      }
    EOGQL
  end

  def execute_graphql(query)
    puts '==='
    puts group_full_path
    response = HTTParty.post(
      GRAPHQL_API_URL,
      body: { query: query }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
    JSON.parse(response.body)
  end

  def update_linked_issue_label(project_id, merge_request_iid, already_labelled, has_linked_issue)
    return if already_labelled == has_linked_issue

    body = has_linked_issue ? "/label ~#{LINKED_ISSUE_LABEL}" : "/unlabel ~#{LINKED_ISSUE_LABEL}"
    puts " #{body}"

    return unless PRODUCTION

    response = HTTParty.post(
      "#{REST_API_URL}/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes",
      body: { body: body }.to_json,
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{GITLAB_API_TOKEN}" }
    )
    puts " #{response.code}"
    @error = true unless response.success?
  end

  def linked_issue?(web_url)
    response = HTTParty.get(web_url)
    mr_widget_data_string = response.lines.select { |line| line.include?('window.gl.mrWidgetData =') }
    mr_widget_data = JSON.parse(mr_widget_data_string.first.slice(25..-1))
    mr_widget_data.dig('issues_links', 'closing_count').to_i.positive? || mr_widget_data.dig('issues_links', 'mentioned_count').to_i.positive?
  end

  def execute
    mrs = execute_graphql(graphql_query).dig('data', 'group', 'mergeRequests', 'nodes')
    puts "Found mrs: #{mrs.count}"
    @error = true if mrs.count == 100

    mrs.each do |mr|
      web_url = mr['webUrl']
      puts web_url
      already_labelled = mr.dig('labels', 'nodes').any? { |label| label['title'] == LINKED_ISSUE_LABEL }
      puts " Already labelled: #{already_labelled}"
      has_linked_issue = linked_issue?(web_url)
      puts " Issue: #{has_linked_issue}"
      update_linked_issue_label(mr['projectId'], mr['iid'], already_labelled, has_linked_issue)
    end

    @error
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  # We want to allow time for the job to setup (pull docker image etc)
  # and also to allow for an occasional failure.
  buffer_minutes = 11 * 60
  updated_after = Time.now.utc - buffer_minutes
  puts "Grabbing mrs updated since #{updated_after}"

  error = false
  error = true if MrLinkedIssueLabeller.new('gitlab-org', updated_after).execute
  error = true if MrLinkedIssueLabeller.new('gitlab-com', updated_after).execute

  puts '==='
  puts "Done in #{Time.now - start} seconds."

  raise 'Something went wrong!' if error
end
